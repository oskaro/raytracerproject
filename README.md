# What is this project?

I wanted to learn more about the Rust language, because I feel like it is an intriguing language
with features that I value. So instead of doing some freestyle project I found the
[Ray tracer in one weekend](https://www.realtimerendering.com/raytracing/Ray%20Tracing%20in%20a%20Weekend.pdf)
project and decided that this was a cool project to learn Rust.

## Goals

I wanted to learn the trait system, general syntax and how modules works when I started this project
and I have definitely learned a lot about the language, so overall a great project.

## Things I have learnt

How ray tracers work, at least a basic understanding, how to do concurrent programming in Rust.