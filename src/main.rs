extern crate image;
extern crate rand;

mod camera;
mod material;
mod objects;
mod ray;
mod render;
mod util;
mod vec3;

use camera::Camera;
use render::{async_renderer, sync_renderer};
use std::io::prelude::*;
use util::make_random_scene;
use vec3::Vec3;

const BENCHMARK_FILE: &str = "./benchmarks/benchmark_overnight.csv";

fn main() {
    // Our camera is at origo and the coordinate frame
    // is as follows:
    // negative z is "out" of the camera
    // x is to the right of the camera
    // y is upwards from the camera
    let anti_aliasing_samples: u32 = 100;
    let benchmark = true;

    let world = std::sync::Arc::new(make_random_scene());

    let lookfrom = 5.0 * Vec3::new(3.0, 3.0, 2.0);
    let lookat = Vec3::new(0.0, 0.0, -1.0);
    let dist_to_focus = (lookfrom - lookat).length();
    let aperture = 0.0;
    let mut height = 16 * 56;
    let mut width = 9 * 56;

    if !benchmark {
        let k = 240;
        let n_x = k * 16;
        let n_y = k * 9;
        let cam = Camera::new(
            lookfrom,
            lookat,
            Vec3::new(0.0, 1.0, 0.0),
            20.0,
            n_x as f32 / n_y as f32,
            aperture,
            dist_to_focus,
        );
        let img = async_renderer::render(n_x, n_y, anti_aliasing_samples, cam, world.clone())
            .expect("Didn't manage to render scene");
        img.save("./data/render.png").expect("Could not save image");
    } else {
        let mut fh = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(BENCHMARK_FILE)
            .expect("Cannot open file");
        writeln!(fh, "Aspect Ratio,Concurrent,Non-Concurrent").expect("Could not open file!");
        loop {
            let n_x = height;
            let n_y = width;
            println!("Rendering {}x{}", n_x, n_y);
            let cam = Camera::new(
                lookfrom,
                lookat,
                Vec3::new(0.0, 1.0, 0.0),
                20.0,
                n_x as f32 / n_y as f32,
                aperture,
                dist_to_focus,
            );

            use std::time::Instant;
            let now = Instant::now();
            let async_time =
                match async_renderer::render(n_x, n_y, anti_aliasing_samples, cam, world.clone()) {
                    Ok(_) => {
                        println!("Async finished after: {} seconds", now.elapsed().as_secs());
                        now.elapsed().as_secs()
                    }
                    Err(err) => {
                        panic!(err);
                    }
                };

            let now = Instant::now();
            let sync_time =
                match sync_renderer::render(n_x, n_y, anti_aliasing_samples, cam, world.clone()) {
                    Ok(_) => {
                        println!("Sync finished after: {} seconds", now.elapsed().as_secs());
                        now.elapsed().as_secs()
                    }
                    Err(err) => {
                        panic!(err);
                    }
                };
            let mut fh = std::fs::OpenOptions::new()
                .create(true)
                .append(true)
                .write(true)
                .open(BENCHMARK_FILE)
                .expect("Cannot open file");
            writeln!(fh, "{}x{},{}s,{}s", n_x, n_y, async_time, sync_time)
                .expect("Couldn not write to file!");

            height += 16;
            width += 9;
        }
    }
}
