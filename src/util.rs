use crate::material::materials::{Dielectric, Lambertian, Metal};
use crate::objects::hitable::HitableList;
use crate::objects::sphere::Sphere;
use crate::vec3::Vec3;
use rand::Rng;

pub fn make_random_scene() -> HitableList {
    let mut list = HitableList::new();
    list.push(Box::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Box::new(Lambertian::new(Vec3::new(0.5, 0.5, 0.5))),
    )));

    let mut gen = rand::thread_rng();
    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = gen.gen::<f32>();
            let center = Vec3::new(
                a as f32 + 0.9 * gen.gen::<f32>(),
                0.2,
                b as f32 + 0.9 * gen.gen::<f32>(),
            );
            if (center - Vec3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_mat < 0.8 {
                    // Diffuse
                    list.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Lambertian::new(Vec3::new(
                            gen.gen::<f32>().powf(2.0),
                            gen.gen::<f32>().powf(2.0),
                            gen.gen::<f32>().powf(2.0),
                        ))),
                    )));
                } else if choose_mat < 0.95 {
                    // Metallic
                    list.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Metal::new(
                            Vec3::new(
                                0.5 * (1.0 + gen.gen::<f32>()),
                                0.5 * (1.0 + gen.gen::<f32>()),
                                0.5 * gen.gen::<f32>(),
                            ),
                            0.5 * gen.gen::<f32>(),
                        )),
                    )));
                } else {
                    // Glass
                    list.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Dielectric::new(1.5)),
                    )));
                }
            }
        }
    }
    list.push(Box::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        Box::new(Dielectric::new(1.5)),
    )));
    list.push(Box::new(Sphere::new(
        Vec3::new(-4.0, 1.0, 0.0),
        1.0,
        Box::new(Lambertian::new(Vec3::new(0.4, 0.2, 0.1))),
    )));
    list.push(Box::new(Sphere::new(
        Vec3::new(4.0, 1.0, 0.0),
        1.0,
        Box::new(Metal::new(Vec3::new(0.7, 0.6, 0.5), 0.0)),
    )));
    list
}
