use crate::objects::hitable::HitRecord;
use crate::ray::Ray;
use crate::vec3::Vec3;

pub mod materials;
mod util;

pub trait Material: Send + Sync {
    /// scatter calculates if a ray is scattered based on a hit record.
    /// The result will be put in the attenuation and scattered parameters.
    fn scatter(&self, ray_in: &Ray, hit_record: &HitRecord) -> Option<(Vec3, Ray)>;
}

/// NoMaterial is a default material for creating default dynamic Material trait objects.
pub struct NoMaterial;

impl Material for NoMaterial {
    fn scatter(&self, _ray_in: &Ray, _hit_record: &HitRecord) -> Option<(Vec3, Ray)> {
        None
    }
}
