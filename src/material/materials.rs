use super::util::{reflect, refract, shlick};
use super::Material;
use crate::objects::hitable::HitRecord;
use crate::objects::sphere::random_in_unit_sphere;
use crate::ray::Ray;
use crate::vec3::Vec3;

extern crate rand;
use rand::Rng;

#[derive(Clone, Copy)]
pub struct Lambertian {
    albedo: Vec3,
}

impl Lambertian {
    pub fn new(a: Vec3) -> Self {
        Lambertian { albedo: a }
    }
}

impl Material for Lambertian {
    fn scatter(&self, _ray_in: &Ray, hit_record: &HitRecord) -> Option<(Vec3, Ray)> {
        let target = hit_record.p + hit_record.normal + random_in_unit_sphere();
        Some((
            self.albedo.clone(),
            Ray::from(hit_record.p, target - hit_record.p),
        ))
    }
}

#[derive(Clone, Copy)]
pub struct Metal {
    albedo: Vec3,
    fuzz: f32,
}

impl Metal {
    pub fn new(a: Vec3, f: f32) -> Self {
        let fuzz = if f < 1.0 { f } else { 1.0 };
        Self {
            albedo: a,
            fuzz: fuzz,
        }
    }
}

impl Material for Metal {
    fn scatter(&self, ray_in: &Ray, hit_record: &HitRecord) -> Option<(Vec3, Ray)> {
        let reflected = reflect(Vec3::unit_vector(ray_in.direction()), hit_record.normal);
        let scattered = Ray::from(
            hit_record.p,
            reflected + self.fuzz * random_in_unit_sphere(),
        );

        if Vec3::dot(scattered.direction(), hit_record.normal) > 0.0 {
            return Some((self.albedo.clone(), scattered));
        }

        None
    }
}

pub struct Dielectric {
    ref_idx: f32,
}

impl Dielectric {
    pub fn new(idx: f32) -> Self {
        Dielectric { ref_idx: idx }
    }
}

impl Material for Dielectric {
    fn scatter(&self, ray_in: &Ray, hit_record: &HitRecord) -> Option<(Vec3, Ray)> {
        let attenuation = Vec3::new(1.0, 1.0, 1.0);

        let (outward_normal, ni_over_nt, cosine) =
            if Vec3::dot(ray_in.direction(), hit_record.normal) > 0.0 {
                (
                    -hit_record.normal,
                    self.ref_idx,
                    self.ref_idx * Vec3::dot(ray_in.direction(), hit_record.normal)
                        / ray_in.direction().length(),
                )
            } else {
                (
                    hit_record.normal,
                    1.0 / self.ref_idx,
                    -Vec3::dot(ray_in.direction(), hit_record.normal) / ray_in.direction().length(),
                )
            };

        let (reflect_prob, refracted) =
            if let Some(refracted) = refract(ray_in.direction(), outward_normal, ni_over_nt) {
                (shlick(cosine, self.ref_idx), refracted)
            } else {
                (1.0, Vec3::new(0.0, 0.0, 0.0))
            };

        if rand::thread_rng().gen::<f32>() < reflect_prob {
            return Some((
                attenuation,
                Ray::from(hit_record.p, reflect(ray_in.direction(), hit_record.normal)),
            ));
        }

        Some((attenuation, Ray::from(hit_record.p, refracted)))
    }
}
