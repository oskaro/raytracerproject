use std::fmt;
use std::ops;

#[derive(Debug, PartialEq, Clone, Copy, Default)]
pub struct Vec3 {
    e: [f32; 3],
}

impl Vec3 {
    pub fn new(e1: f32, e2: f32, e3: f32) -> Self {
        Self { e: [e1, e2, e3] }
    }

    pub fn length(self) -> f32 {
        self.e.iter().map(|x| x * x).sum::<f32>().sqrt()
    }

    pub fn length_squared(&self) -> f32 {
        self.e.iter().map(|x| x * x).sum::<f32>()
    }

    pub fn unit_vector(v: Self) -> Self {
        let l = v.length();
        v / l
    }

    pub fn cross(u: Self, v: Self) -> Self {
        Vec3::new(
            u.y() * v.z() - u.z() * v.y(),
            u.z() * v.x() - u.x() * v.z(),
            u.x() * v.y() - u.y() * v.x(),
        )
    }

    pub fn dot(lhs: Self, rhs: Self) -> f32 {
        lhs.e
            .iter()
            .zip(rhs.e.iter())
            .map(|it| it.0 * it.1)
            .sum::<f32>()
    }

    pub fn r(self) -> f32 {
        self.e[0]
    }
    pub fn g(self) -> f32 {
        self.e[1]
    }
    pub fn b(self) -> f32 {
        self.e[2]
    }
    pub fn x(self) -> f32 {
        self.e[0]
    }
    pub fn y(self) -> f32 {
        self.e[1]
    }
    pub fn z(self) -> f32 {
        self.e[2]
    }
}

impl fmt::Display for Vec3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{} {} {}]", self.e[0], self.e[1], self.e[2])
    }
}

impl ops::Add for Vec3 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Vec3::new(
            self.e[0] + rhs.e[0],
            self.e[1] + rhs.e[1],
            self.e[2] + rhs.e[2],
        )
    }
}

impl ops::AddAssign for Vec3 {
    fn add_assign(&mut self, other: Self) {
        self.e[0] += other.e[0];
        self.e[1] += other.e[1];
        self.e[2] += other.e[2];
    }
}

impl ops::Sub for Vec3 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec3::new(
            self.e[0] - rhs.e[0],
            self.e[1] - rhs.e[1],
            self.e[2] - rhs.e[2],
        )
    }
}

impl ops::Neg for Vec3 {
    type Output = Self;
    fn neg(self) -> Self::Output {
        Vec3::new(-self.e[0], -self.e[1], -self.e[2])
    }
}

impl ops::Mul for Vec3 {
    type Output = Self;
    fn mul(self, v: Self) -> Self::Output {
        Vec3::new(self.e[0] * v.e[0], self.e[1] * v.e[1], self.e[2] * v.e[2])
    }
}
impl ops::Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, c: f32) -> Self::Output {
        Vec3::new(c * self.e[0], c * self.e[1], c * self.e[2])
    }
}
impl ops::Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Self::Output {
        Vec3::new(self * rhs.x(), self * rhs.y(), self * rhs.z())
    }
}

impl ops::Div<f32> for Vec3 {
    type Output = Self;

    fn div(self, c: f32) -> Self::Output {
        Vec3::new(self.e[0] / c, self.e[1] / c, self.e[2] / c)
    }
}

impl ops::DivAssign<f32> for Vec3 {
    fn div_assign(&mut self, rhs: f32) {
        self.e[0] /= rhs;
        self.e[1] /= rhs;
        self.e[2] /= rhs;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    pub fn test_create_new_vec3() {
        let _v = Vec3::new(1.0, 1.0, 1.0);
    }

    #[test]
    pub fn test_add_vec3s() {
        assert_eq!(
            Vec3::new(1.0, 1.0, 1.0) + Vec3::new(1.0, 1.0, 1.0),
            Vec3::new(2.0, 2.0, 2.0)
        )
    }

    #[test]
    pub fn test_multiplying_with_a_scalar() {
        assert_eq!(Vec3::new(1.0, 1.0, 1.0) * 10.0, Vec3::new(10.0, 10.0, 10.0));
    }

    #[test]
    pub fn test_divide_by_a_scalar() {
        assert_eq!(Vec3::new(10.0, 10.0, 10.0) / 10.0, Vec3::new(1.0, 1.0, 1.0));
        assert_eq!(Vec3::new(5.0, 15.0, 20.0) / 5.0, Vec3::new(1.0, 3.0, 4.0));
    }

    #[test]
    pub fn test_length_of_a_vector() {
        assert_eq!(Vec3::new(1.0, 0.0, 0.0).length(), 1.0);
        assert_eq!(Vec3::new(0.0, 1.0, 0.0).length(), 1.0);
        assert_eq!(Vec3::new(0.0, 0.0, 1.0).length(), 1.0);
        assert_eq!(Vec3::new(1.0, 1.0, 1.0).length(), (3.0 as f32).sqrt());
        assert_eq!(
            Vec3::new(100.0, 100.0, 100.0).length(),
            (30000.0 as f32).sqrt()
        );
    }

    #[test]
    pub fn test_unit_vectors_are_of_length_1() {
        let v = Vec3::new(100.0, 100.0, 100.0);
        assert_eq!(v.r(), 100.0);
        assert_eq!(v.g(), 100.0);
        assert_eq!(v.b(), 100.0);
        let v = Vec3::unit_vector(v);
        assert!((v.length() - 1.0).abs() < 0.0000001);
    }
    #[test]
    pub fn test_dot_product() {
        let v1 = Vec3::new(1.0, 1.0, 1.0);
        let v2 = Vec3::new(2.0, 2.0, 2.0);
        assert_eq!(Vec3::dot(v1, v2), 6.0);
    }
}
