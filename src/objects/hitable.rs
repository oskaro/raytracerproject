use crate::material::{Material, NoMaterial};
use crate::ray::Ray;
use crate::vec3::Vec3;
use std::boxed::Box;
use std::sync::Arc;

pub struct HitRecord {
    pub t: f32,
    pub p: Vec3,
    pub normal: Vec3,
    pub mat: Arc<Box<dyn Material>>,
}

impl Default for HitRecord {
    fn default() -> Self {
        HitRecord {
            t: 0.0,
            p: Vec3::new(0.0, 0.0, 0.0),
            normal: Vec3::new(0.0, 0.0, 0.0),
            mat: Arc::new(Box::new(NoMaterial {})),
        }
    }
}

pub trait Hitable: Send + Sync {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord>;
}

pub type HitableList = Vec<Box<dyn Hitable>>;

impl Hitable for HitableList {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let mut closest_hit: Option<HitRecord> = None;
        let mut closest = t_max;
        for h in self.iter() {
            if let Some(hit) = h.hit(r, t_min, closest) {
                closest = hit.t;
                closest_hit = Some(hit);
            }
        }
        closest_hit
    }
}
