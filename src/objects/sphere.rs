use super::hitable::{HitRecord, Hitable};
use crate::material::Material;
use crate::ray::Ray;
use crate::vec3::Vec3;
extern crate rand;
use rand::Rng;

pub struct Sphere {
    center: Vec3,
    radius: f32,
    material: std::sync::Arc<Box<dyn Material>>,
}

impl Sphere {
    pub fn new(center: Vec3, radius: f32, material: Box<dyn Material>) -> Self {
        assert!(radius != 0.0);
        Sphere {
            center: center,
            radius: radius,
            material: std::sync::Arc::from(material),
        }
    }
}

impl Hitable for Sphere {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let oc = r.origin() - self.center;
        let a = Vec3::dot(r.direction(), r.direction());
        let b = Vec3::dot(oc, r.direction());
        let c = Vec3::dot(oc, oc) - self.radius * self.radius;
        let discriminant = b * b - a * c;
        if discriminant > 0.0 {
            let root = (-b - discriminant.sqrt()) / a;
            if root < t_max && root > t_min {
                let p = r.point_at_param(root);
                return Some(HitRecord {
                    t: root,
                    p: p,
                    normal: (p - self.center) / self.radius,
                    mat: std::sync::Arc::clone(&self.material),
                });
            }
            let root = (-b + discriminant.sqrt()) / a;
            if root < t_max && root > t_min {
                let p = r.point_at_param(root);
                return Some(HitRecord {
                    t: root,
                    p: p,
                    normal: (p - self.center) / self.radius,
                    mat: std::sync::Arc::clone(&self.material),
                });
            }
        }
        return None;
    }
}

pub fn random_in_unit_sphere() -> Vec3 {
    let mut gen = rand::thread_rng();
    let mut p = 2.0 * Vec3::new(gen.gen(), gen.gen(), gen.gen()) - Vec3::new(1.0, 1.0, 1.0);
    while p.length_squared() >= 1.0 {
        p = 2.0 * Vec3::new(gen.gen(), gen.gen(), gen.gen()) - Vec3::new(1.0, 1.0, 1.0);
    }
    p
}
