use super::color;
use crate::camera::Camera;
use crate::objects::hitable::HitableList;
use crate::vec3::Vec3;
use image::{Rgb, RgbImage};

use rand::Rng;

pub fn render(
    n_x: u32,
    n_y: u32,
    n_s: u32,
    cam: Camera,
    world: std::sync::Arc<HitableList>,
) -> Result<RgbImage, String> {
    let mut rng = rand::thread_rng();
    // The RGB image is a nice way to output the image
    // as a PNG and not the PPM which is used in the tutorial
    let mut progress = 0;
    let img = image::RgbImage::from_fn(n_x, n_y, |x, y| {
        let mut c = Vec3::new(0.0, 0.0, 0.0);

        for _ in 0..n_s {
            let u = (x as f32 + rng.gen::<f32>()) / n_x as f32;
            let v = ((n_y - y) as f32 + rng.gen::<f32>()) / n_y as f32;
            let r = cam.get_ray(u, v);
            c += color(r, &world, 0);
        }

        // Normalize it over the number of blended rays.
        c /= n_s as f32;
        progress += 1;
        print!(
            "{:.3}% done\r",
            100.0 * (progress as f32) / (n_x as f32 * n_y as f32)
        );
        Rgb([
            (255.99 * c.r().sqrt()) as u8,
            (255.99 * c.g().sqrt()) as u8,
            (255.99 * c.b().sqrt()) as u8,
        ])
    });

    Ok(img)
}
