use super::color;
use super::RenderError;
use crate::camera::Camera;
use crate::objects::hitable::HitableList;
use crate::rand::Rng;
use crate::vec3::Vec3;

extern crate image;
extern crate num_cpus;
extern crate threadpool;

use image::{ImageBuffer, Rgb, RgbImage};
use std::sync::mpsc::channel;
use threadpool::ThreadPool;

pub fn render(
    n_x: u32,
    n_y: u32,
    n_s: u32,
    cam: Camera,
    world: std::sync::Arc<HitableList>,
) -> Result<RgbImage, RenderError> {
    let mut img = ImageBuffer::new(n_x, n_y);
    let pool = ThreadPool::new(num_cpus::get());

    let (tx, rx) = channel();

    for y in 0..n_y {
        let tx = tx.clone();
        let world = world.clone();
        pool.execute(move || {
            let mut rng = rand::thread_rng();
            for x in 0..n_x {
                let mut c = Vec3::new(0.0, 0.0, 0.0);

                for _ in 0..n_s {
                    let u = (x as f32 + rng.gen::<f32>()) / n_x as f32;
                    let v = ((n_y - y) as f32 + rng.gen::<f32>()) / n_y as f32;
                    let r = cam.get_ray(u, v);
                    c += color(r, &world, 0);
                }

                // Normalize it over the number of blended rays.
                c /= n_s as f32;

                let px = Rgb([
                    (255.99 * c.r().sqrt()) as u8,
                    (255.99 * c.g().sqrt()) as u8,
                    (255.99 * c.b().sqrt()) as u8,
                ]);

                tx.send((x, y, px)).expect("Could not create pixel!");
            }
        })
    }
    let total = n_x * n_y;
    for progress in 0..total {
        let (x, y, pixel) = match rx.recv() {
            Ok((x, y, pixel)) => (x, y, pixel),
            Err(_) => return Err(RenderError::AsyncError),
        };
        img.put_pixel(x, y, pixel);
        print!("{:.3}% done\r", 100.0 * progress as f32 / total as f32);
    }

    return Ok(img);
}
