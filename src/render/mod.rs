use crate::objects::hitable::{Hitable, HitableList};
use crate::ray::Ray;
use crate::vec3::Vec3;

pub mod async_renderer;
pub mod sync_renderer;

#[derive(Debug)]
pub enum RenderError {
    AsyncError,
}

fn color(r: Ray, world: &HitableList, depth: i32) -> Vec3 {
    if let Some(rec) = world.hit(r, 0.0001, std::f32::MAX) {
        match (depth < 50, rec.mat.scatter(&r, &rec)) {
            (true, Some((attenuation, scattered))) => {
                attenuation * color(scattered, world, depth + 1)
            }
            (_, None) | (false, _) => Vec3::new(0.0, 0.0, 0.0),
        }
    } else {
        let unit_dir = Vec3::unit_vector(r.direction());
        let t = 0.5 * (unit_dir.y() + 1.0);
        Vec3::new(1.0, 1.0, 1.0) * (1.0 - t) + Vec3::new(0.5, 0.7, 1.0) * t
    }
}
