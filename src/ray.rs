use crate::vec3::Vec3;

#[derive(Debug, Clone, Copy)]
pub struct Ray {
    a: Vec3,
    b: Vec3,
}

impl Ray {
    pub fn from(a: Vec3, b: Vec3) -> Ray {
        Ray { a: a, b: b }
    }

    pub fn origin(self) -> Vec3 {
        self.a
    }

    pub fn direction(self) -> Vec3 {
        self.b
    }

    pub fn point_at_param(self, t: f32) -> Vec3 {
        self.a + self.b * t
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn create_ray_from_vec3s() {
        let o = Vec3::new(0.0, 0.0, 0.0);
        let d = Vec3::new(1.0, 1.0, 1.0);
        let _r = Ray::from(o, d);
    }
}
